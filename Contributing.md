# This file describes the steps towards contribution

The current file structure is to have a clear division between business logic(state handling, API calls, etc.) and purely UI components. In addition, further structuring is done for separating different pages.

To contribute to the website, working knowledge of ReactJS, styled-components is necessary. Follow the guidelines given below to contribute.

---

## Guidelines

1. Look at a current issue that you are looking to work toward fixing. Create one with appropriate flags if the issue doesn't exist.
2. Create a pull request and ensure that commits follow the format given here.
3. Before merging the pull request, ensure that the code has been formatted based on the [prettier config file](.prettierrc.json) and has passed the linter as well with rules defined in [eslint config file](.eslintrc.json). Both can be installed using either using NPM or using the code editors themselves.
4. Ensure that commit messages are formatted in the following [format](https://gitlab.com/nitk-nest/nest/-/blob/master/CONTRIBUTING.md#git-commit-messages)

---
