const colors = {
    primaryLight: "#ffffff",
    secondaryLight: "#2b2b2b",
    primaryDark: "#00081d",
    secondaryDark: "#0ab6c2",
    headerHoverDark: "#FF0000",
    headerHoverLight: "#00b418",
    loaderLight: "#c0c0c0",
    loaderDark: "#a0a0a0",
    modalBackground: "#ffffff",
    overlayBackground: "#00000066",
    terminalBackground: "#2b2b2b",
    terminalColor: "#ffffff",
    NavbarPrimary: "#2b2b2b",
    NavbarSecondary: "#fafafa",
    SecondaryAlt: "#414a4c",
};
export default colors;
