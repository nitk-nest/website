const links = {
    links: [
        { name: "Home", link: "/" },
        { name: "Announcements", link: "/announcements" },
        {
            name: "Getting Started",
            link: "https://nest.nitk.ac.in/docs/v0.4/user/tutorial.html",
            isExternal: true,
        },
        { name: "Events", link: "/events" },
        { name: "Contributors", link: "/contributors" },
    ],
};
export default links;
