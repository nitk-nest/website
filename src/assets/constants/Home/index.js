import logo from "../../images/NestLogo.png";
import main from "../../images/NestMainHeader.png";
import arch from "../../images/NeSTArch.png";

export default {
    logo: logo,
    main: main,
    archImage: arch,
};
