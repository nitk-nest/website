import React from "react";
import ExternalLink from "../../../components/UI/Utility/ExternalLink";
export default [
    {
        name: "Presented a talk at IndiaFOSS 2.0 Conference",
        desc: (
            <>
                <p>
                    The 2nd edition of Free and Open Source Software conference
                    by the FOSS United community.
                </p>
                <p>
                    IndiaFOSS is a community event. The goal is to bring
                    software developers, policy makers,communities and
                    enthusiasts together, so we can learn and inspire each
                    other.
                </p>
            </>
        ),
        date: "2022-07-24",
        link: "https://indiafoss.net/",
    },
    {
        name: "Presented a lightning talk at WNS3",
        desc: (
            <>
                The{" "}
                <ExternalLink
                    link="https://www.nsnam.org/research/wns3/wns3-2021/"
                    isStyled
                >
                    WNS3
                </ExternalLink>{" "}
                is traditionally part of a week-long annual meeting for ns-3,
                including training and other activities related to ns-3 use and
                development, including the annual meeting of the ns-3
                Consortium. The objective of the workshop is to gather ns-3
                users and developers, together with networking simulation
                practitioners and users, and developers of other network
                simulation tools, to learn about advances pertaining to the ns-3
                simulator.
            </>
        ),
        date: "2021-06-23",
        link: "https://www.nsnam.org/research/wns3/wns3-2021/program/#:~:text=Narayan%20G%2C%20Dhanasekar%20M%2C%20Shanthanu%20S%20Rai%2C%20Leslie%20Monis%2C%20Mohit%20P.%20Tahiliani%2C%20Validating%20Congestion%20Control%20Mechanisms%20and%20Queue%20Disciplines%20in%20ns-3%20using%20NeST",
    },
    {
        name: "Took part in NITK ONE-2020 workshop",
        desc: (
            <>
                Wireless Information Networking Group (WiNG) at NITK Surathkal
                focuses on research in wired and wireless networked systems.
                Open-source Network Experimentation (ONE) is an annual workshop
                conducted by WiNG. The most active research areas include:
                Network Simulations and Emulations, Linux Queue Disciplines, and
                Routing, Localization and Security in Underwater Communications.
            </>
        ),
        date: "2020-12-14",
        link: "https://www.nitk.ac.in/document/attachments/2146/ONE-2020.pdf",
    },
    {
        name: "Blog about NeST published in APNIC",
        desc: (
            <>
                <ExternalLink link="https://www.apnic.net/" isStyled>
                    APNIC
                </ExternalLink>{" "}
                (Asia Pacific Network Information Centre, pronounced A-P-NIC) is
                an open, member-based, not-for-profit organization, whose
                primary role is to distribute and manage Internet number
                resources (IP addresses and AS numbers) in the Asia Pacific
                region’s 56 economies.
            </>
        ),
        date: "2020-09-18",
        link: "https://blog.apnic.net/2020/09/18/nest-a-simpleefficient-tool-to-study-congestion-control/",
    },
    {
        name: "Presentation at RFC’s we love August edition",
        desc: (
            <>
                <ExternalLink
                    link="https://www.iiesoc.in/post/rfcs-we-love-aug-2020"
                    isStyled
                >
                    RFCs we love
                </ExternalLink>{" "}
                is a meetup for technical discussions around specifications and
                standards. It is inspired by the paperswelove community, but
                focuses on technical standards instead of research papers.
            </>
        ),
        date: "2020-08-28",
        link: "https://youtu.be/syGrpoOaqXI?t=2264",
    },
    {
        name: "NeST paper presented at ANRW ‘20",
        desc: (
            <>
                The ACM/IRTF Applied Networking Research Workshop 2020 (
                <ExternalLink link="https://irtf.org/anrw/2020/" isStyled>
                    ANRW’20
                </ExternalLink>
                ) is an academic workshop that provides a forum for researchers,
                vendors, network operators, and the Internet standards community
                to present and discuss emerging results in applied networking
                research, and to find inspiration from topics and open problems
                discussed at the IETF. Checkout the presentation{" "}
                <ExternalLink
                    link="https://youtu.be/VhC7suupR7E?t=1795"
                    isStyled
                >
                    video
                </ExternalLink>{" "}
                and{" "}
                <ExternalLink
                    link="https://irtf.org/anrw/2020/slides-narayan-g-00.pdf"
                    isStyled
                >
                    slides
                </ExternalLink>
                .
            </>
        ),
        date: "2020-07-30",
        link: "https://dl.acm.org/doi/abs/10.1145/3404868.3406670",
    },
    {
        name: "Conducted a webinar in NMAMIT",
        desc: (
            <>
                NMAMIT (NMAM Institute of Technology) held a webinar on Network
                Emulation using Linux Namespaces and NeST, where we presented
                our work for the first time.
            </>
        ),
        date: "2020-07-16",
    },
];
