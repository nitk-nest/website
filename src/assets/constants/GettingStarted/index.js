import React from "react";
import ExternalLink from "../../../components/UI/Utility/ExternalLink";
import img1 from "../../images/udp1.png";
import img2 from "../../images/udp2.png";
import img3 from "../../images/n1SendingRate.png";
import img4 from "../../images/ping.png";
import img5 from "../../images/socketStats.png";
import img6 from "../../images/trafficControl.png";
import img7 from "../../images/trafficControl1.png";
import img8 from "../../images/topology.jpeg";

const gettingStarted = {
    carouselData: [
        {
            img: img1,
            alt: "UDP Flow",
            txt: "First UDP flow from Node 1 to Node 2",
        },
        {
            img: img2,
            alt: "UDP Flow",
            txt: "Second UDP flow from Node 1 to Node 2",
        },
        {
            img: img3,
            alt: "TCP Flow",
            txt: "These plots show both the TCP flows. Note how TCP flows take upto 50 Mbit, since the other 50 Mbit is consumed by UDP flows.",
        },
        {
            img: img4,
            alt: "Ping",
            txt: "Slightly more than 30ms.",
        },
        {
            img: img5,
            alt: "CWND of TCP Flows",
            txt: "TCP Cubic is used.",
        },
        {
            img: img6,
            alt: "Queue length",
            txt: "CoDel Qdisc. Note that it looks similar to ping plot, since in this scenario the Queue at the router mainly dictates the latency in the network.",
        },
        {
            img: img7,
            alt: "Queue length",
            txt: "Packet drops at the queue.",
        },
    ],
    installationData: {
        heading: "Install NeST",
        requirementsPython:
            "First ensure that you have Python version 3.6 or above:",
        requirementsPythonCmd: "python3 -V",
        requirementsPip: (
            <>
                Ensure that you have pip (version &gt;= 9) installed. For eg.,
                on Ubuntu, run:
            </>
        ),
        requirementsPipCmd: "sudo apt install python3-pip",
        requirementsPipUpdate:
            "After installing pip, make sure you also upgrade it:",
        requirementsPipUpdateCmd: "python3 -m pip install -U pip",
        dependencies: (
            <>
                To install other dependecies, refer this{" "}
                <ExternalLink
                    link="https://nest.nitk.ac.in/docs/v0.4.1/user/install.html#install-dependencies"
                    isStyled
                >
                    link
                </ExternalLink>
                .
            </>
        ),
        note: "NeST is supported for Linux systems only.",
        general: [
            {
                txt: "First, ensure that you have pip installed to install python3 packages.It can be installed from your Linux Package Manager. For example, this is the command to install pip in Ubuntu:",
                code: "sudo apt install python3-pip",
            },
            {
                txt: "After installing pip, make sure you also upgrade it:",
                code: "python3 -m pip install -U pip",
            },
        ],
        pyPi: {
            txt: "This is the recommended way to install NeST for most users:",
            code: "python3 -m pip install nest",
        },
        source: {
            txt1: "Follow this approach if you want the latest source code with unreleased features:",
            meth1: {
                txt: "Clone the repository",
                code: "git clone https://gitlab.com/nitk-nest/nest.git",
            },
            txt2: "cd into the 'nest' directory and install via pip",
            meth2: {
                code: "python3 -m pip install .",
            },
        },
    },
    introData: {
        txt1: "NeST is a python3 package aiding researchers and beginners alike in emulating real-world networks. Here is a small peek into the APIs NeST provides.",
        api: [
            "Creation of network testbed",
            "Configuration of network testbed",
            "Setup network experiment on the testbed",
            "Collection and Visualization of the data from the network experiment",
        ],
        txt2: "NeST is officially supported for Python 3.6+.",
    },
    topo: {
        txt: "CoDel Qdisc is installed at the router. There are 2 TCP flows and 2 UDP flows from Node 0 to Node 1. The target bandwidth of each of the 2 UDP flow is 25Mbits. So, both UDP flows take up 50 Mbit bandwidth, and the remaining 50 Mbit bandwidth is shared between the 2 TCP flows. The experiment is running for 60s.",
        img: img8,
    },
};
export default gettingStarted;
