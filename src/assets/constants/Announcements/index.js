import React from "react";
import ExternalLink from "../../../components/UI/Utility/ExternalLink";
export default [
    {
        announcementId: 15,
        announcementHeading: "NeST (v0.4) released!",
        announcementDesc: (
            <>
                Release date: February 16, 2022
                <br></br>
                <br></br>
                This release adds new and exciting features such as:
                <ul>
                    <li>IPv4/Ipv6 Address helpers</li>
                    <li>Switch support</li>
                    <li>Toggling offloads</li>
                    <li>Enhanced examples</li>
                </ul>
                and many more.
                <br></br>
                <br></br>
                <ExternalLink
                    isStyled
                    link="https://gitlab.com/nitk-nest/nest/-/releases/v0.4"
                >
                    Checkout the release notes
                </ExternalLink>
            </>
        ),
    },
    {
        announcementId: 15,
        announcementHeading: "NeST (v0.3) released!",
        announcementDesc: (
            <>
                Release date: April 1, 2021
                <br></br>
                <br></br>
                This release adds new and exciting features such as:
                <ul>
                    <li>IPv6 support</li>
                    <li>FRR routing suite</li>
                    <li>MPLS support</li>
                    <li>APIs to set packet corruption and loss</li>
                    <li>
                        Support for Ldp dynamic routing protocol using FRR
                        routing
                    </li>
                </ul>
                and many more.
                <br></br>
                <br></br>
                <ExternalLink
                    isStyled
                    link="https://gitlab.com/nitk-nest/nest/-/releases/v0.3"
                >
                    Checkout the release notes
                </ExternalLink>
            </>
        ),
    },
    {
        announcementId: 15,
        announcementHeading: "NeST (v0.2.1) released!",
        announcementDesc: (
            <>
                Release date: Dec 13, 2020
                <br></br>
                <br></br>
                Bug fix release for the previous release. With the new addition
                of isis support in Quagga.
                <br></br>
                <br></br>
                <ExternalLink
                    isStyled
                    link="https://gitlab.com/nitk-nest/nest/-/releases/v0.2.1"
                >
                    Checkout the release notes
                </ExternalLink>
            </>
        ),
    },
    {
        announcementId: 15,
        announcementHeading: "NeST (v0.2) released!",
        announcementDesc: (
            <>
                Release date: Nov 03, 2020
                <br></br>
                <br></br>
                This release comes with new features:
                <ul>
                    <li>Dynamic routing</li>
                    <li>Logging support</li>
                    <li>Support for configuring NeST APIs</li>
                </ul>
                and more.
                <br></br>
                <br></br>
                <ExternalLink
                    isStyled
                    link="https://gitlab.com/nitk-nest/nest/-/releases/v0.2"
                >
                    Checkout the release notes
                </ExternalLink>
            </>
        ),
    },
    {
        announcementId: 15,
        announcementHeading: "NeST (v0.1) released!",
        announcementDesc: (
            <>
                Release date: Jul 30, 2020
                <br></br>
                <br></br>
                NeST is public! Providing python APIs to create and manage
                network namespaces, to create a topology, run experiments on the
                built namespaces and plot results.
                <br></br>
                <br></br>
                <ExternalLink
                    isStyled
                    link="https://gitlab.com/nitk-nest/nest/-/releases/v0.1"
                >
                    Checkout the release notes
                </ExternalLink>
            </>
        ),
    },
];
