import { createGlobalStyle, css } from "styled-components";
import theme from "styled-theming";
import colors from "./colors";

const {
    primaryLight,
    secondaryLight,
    secondaryDark,
    primaryDark,
    loaderLight,
    loaderDark,
    terminalBackground,
    terminalColor,
    NavbarPrimary,
    NavbarSecondary,
    SecondaryAlt,
} = colors;

export const terminalTheme = css`
    background-color: ${terminalBackground};
    color: ${terminalColor};
`;

export const loaderThemeColor = theme("mode", {
    light: loaderLight,
    dark: loaderDark,
});

export const primaryThemeColor = theme("mode", {
    light: primaryLight,
    dark: primaryDark,
});

export const secondaryThemeColor = theme("mode", {
    light: secondaryLight,
    dark: secondaryDark,
});

const colorTheme = theme("mode", {
    light: css`
        background-color: ${primaryLight};
        color: ${secondaryLight};
    `,
    dark: css`
        background-color: ${primaryDark};
        color: ${secondaryDark};
    `,
});

export const reverseTheme = theme("mode", {
    light: css`
        background-color: ${SecondaryAlt};
        color: ${primaryLight};
    `,
    dark: css`
        background-color: ${secondaryDark};
        color: ${primaryDark};
    `,
});

export const headerFooterTheme = theme("mode", {
    light: css`
        background-color: ${NavbarSecondary};
        color: ${NavbarPrimary};
    `,
    dark: css`
        background-color: ${secondaryDark};
        color: ${primaryDark};
    `,
});

export const GlobalStyles = createGlobalStyle`
    body{
        margin: 0;
        padding: 0;
        max-width: 100vw;
    }
    #root{
        min-height: 100vh;
    }
    :where(*){
        ${colorTheme};
    }
`;
