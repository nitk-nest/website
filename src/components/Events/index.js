import React from "react";
import { eventData } from "../../assets/constants";
import { ItemContainer, ItemWrapper, Heading } from "./EventUI";
import EventCards from "./EventCards";

const Events = () => {
    const zero = 0;
    const sortedEventData = [...eventData].sort(
        (a, b) => new Date(b.date) - new Date(a.date)
    );

    return (
        <>
            <Heading>Events</Heading>
            <ItemContainer>
                {sortedEventData.length > zero && (
                    <ItemWrapper>
                        {sortedEventData.map((eventItem, index) => {
                            return <EventCards key={index} data={eventItem} />;
                        })}
                    </ItemWrapper>
                )}
            </ItemContainer>
        </>
    );
};
export default Events;
