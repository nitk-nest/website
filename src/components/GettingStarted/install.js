import React, { useState } from "react";
import {
    Content,
    Code,
    Tab,
    CenterContainer,
    TabContainer,
} from "./install.style";
import Heading from "./Heading";

const Install = (props) => {
    const one = 1,
        two = 2;
    const [active, setActive] = useState(one);

    const handleClick = (e) => {
        const i = parseInt(e.target.id, 0);
        if (i !== active) {
            setActive(i);
        }
    };

    return (
        <div>
            <Heading>{props.installationData.heading}</Heading>
            {/* <Content>{props.installationData.requirementsPython}</Content>
            <Code txt={props.installationData.requirementsPythonCmd} /> */}
            <Content>{props.installationData.requirementsPip}</Content>
            <Code txt={props.installationData.requirementsPipCmd} />
            <Content>{props.installationData.requirementsPipUpdate}</Content>
            <Code txt={props.installationData.requirementsPipUpdateCmd} />
            <Content> </Content>

            <CenterContainer display>
                <Tab
                    active={active === one}
                    onClick={(e) => handleClick(e)}
                    id={one}
                >
                    1. From PyPi
                </Tab>
                <Tab
                    active={active === two}
                    onClick={(e) => handleClick(e)}
                    id={two}
                >
                    2. From source
                </Tab>
            </CenterContainer>
            <TabContainer display={active === one}>
                <Content>{props.installationData.pyPi.txt}</Content>
                <Code txt={props.installationData.pyPi.code} />
            </TabContainer>
            <TabContainer display={active === two}>
                {/* <Content>{props.installationData.source.txt1}</Content> */}
                <Content>1. {props.installationData.source.meth1.txt}</Content>
                <Code txt={props.installationData.source.meth1.code} />
                <Content>2. {props.installationData.source.txt2}</Content>
                <Code txt={props.installationData.source.meth2.code} />
            </TabContainer>
            <Content>{props.installationData.dependencies}</Content>
        </div>
    );
};
export default Install;
