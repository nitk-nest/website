import styled from "styled-components";
import React from "react";
export const Container = styled.div`
    width: 70vw;
    margin: 0 15vw 0 15vw;
    @media (max-width: 768px) {
        width: 90vw;
        margin: 0 5vw 0 5vw;
    }
`;
const Separator = styled.div`
    width: 100vw;
    height: 0.15em;
    background-color: currentColor;
`;

export const FlexContainer = ({ hasIndividualBorder, children }) => {
    if (!hasIndividualBorder) return <Flex>{children}</Flex>;
    const one = 1;
    const zero = 0;
    const firstChild = children[zero];
    const childComponents = children.slice(one).map((child) => (
        <React.Fragment key={child}>
            <Separator />
            {child}
        </React.Fragment>
    ));
    return (
        <Flex>
            {firstChild}
            {childComponents}
        </Flex>
    );
};

const Flex = styled.div`
    width: 100%;
    display: flex;
    flex-direction: column;
    justify-content: center;
    align-items: center;
    gap: 1em;
`;
