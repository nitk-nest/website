import styled from "styled-components";
import { reverseTheme } from "../../assets/theme";

export const SlideText = styled.div`
    width: auto;
    padding: 0.5rem 0 0.5rem 0;
`;

export const StyledSlider = styled.div`
    display: flex;
    flex-direction: row;
    justify-content: center;
    align-items: center;
    text-align: center;
`;

export const Arrow = styled.div`
    border: solid currentColor;
    cursor: pointer;
    border-width: 0 0.2em 0.2em 0;
    padding: 0.2em;
    display: inline-block;
    transform: ${(props) => (props.left ? "rotate(135deg)" : "rotate(-45deg)")};
`;

export const StyledPagination = styled.div`
    display: inline-block;
    margin: 2em 0;
    text-align: center;
    justify-content: center;
    align-items: center;
    width: 100%;
`;

export const Dots = styled.span`
    ${({ active }) => (active ? null : reverseTheme)};
    font-size: 1rem;
    cursor: pointer;
    font-weight: 500;
    padding: 0.5em;
    margin: 0 0.5em 0 0.5em;
    align-items: center;
    @media (max-width: 768px) {
        font-size: 0.7rem;
        padding: 0.4em;
    }
`;
