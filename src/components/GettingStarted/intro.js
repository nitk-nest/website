import React from "react";
import styled from "styled-components";
import Heading from "./Heading";

const Content = styled.p`
    padding-left: ${(props) => (props.indent ? "2rem" : "0")};
    span {
        font-weight: 700;
    }
    ol {
        list-style-type: circle;
    }
`;

const Intro = (props) => {
    return (
        <div>
            <Heading>Brief Overview of NeST</Heading>
            <Content>{props.introData.txt1}</Content>
            <Content>
                <span>NeST</span> provides a set of APIs for:
            </Content>
            <ol>
                {props.introData &&
                    props.introData.api.map((child, i) => {
                        return <li key={i}>{child}</li>;
                    })}
            </ol>
            <Content>{props.introData.txt2}</Content>
        </div>
    );
};
export default Intro;
