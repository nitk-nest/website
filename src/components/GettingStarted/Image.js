import styled from "styled-components";

const Image = styled.img`
    width: fit-content;
    height: fit-content;
    @media (max-width: 768px) {
        width: 400px;
        height: auto;
        object-fit: contain;
    }
    @media (max-width: 400px) {
        width: 340px;
        height: auto;
        max-width: 300px;
        object-fit: contain;
    }
`;
export default Image;
