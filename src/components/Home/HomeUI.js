import styled, { css } from "styled-components";
import { Gmail } from "@styled-icons/simple-icons/Gmail";
import { Code } from "@styled-icons/boxicons-regular/Code";
import { Book } from "@styled-icons/boxicons-regular/Book";
import { reverseTheme } from "../../assets/theme";

const iconStyles = css`
    ${reverseTheme};
    > * {
        ${reverseTheme};
    }
    width: 1.5em;
    margin-right: 1em;
`;

export const Docs = styled(Book)`
    ${iconStyles}
`;
export const Source = styled(Code)`
    ${iconStyles}
`;
export const Mail = styled(Gmail)`
    ${iconStyles}
`;

export const FlexContainer = styled.div`
    display: flex;
    flex-direction: ${({ isColumn }) => (isColumn ? "column" : "row")};
    justify-content: center;
    align-items: center;
    gap: 1em;
    margin-top: 1em;
    width: 100%;
    > a {
        border-radius: 0.1em;
        padding: 0.25em 0.5em;
        @media (min-width: 50em) {
            border-radius: 0.2em;
            margin-inline: 3em;
            padding: 0.5em 1em;
            font-weight: 500;
        }
        ${reverseTheme}
    }
    @media (max-width: 50em) {
        flex-direction: column;
    }
`;

export const MainDivForHead = styled.div`
    display: flex;
    width: 75%;
    margin: 0 auto;
    flex-direction: row;
    @media (max-width: 50em) {
        flex-direction: column;
    }
`;
export const MainDiv = styled.div`
    display: flex;
    flex-direction: row;
    width: 80%;
    margin: 2.5em auto;
    box-shadow: 0 0 0.5em 0 currentColor;
    padding: 1em;
    @media (max-width: 50em) {
        flex-direction: column;
        margin-bottom: 1.5em;
    }
`;
export const ImageStyle = styled.img`
    margin: ${({ noAuto }) => (noAuto ? "none" : "auto")};
    margin-top: ${({ isStretch }) => (isStretch ? "1em" : "2em")};
    width: ${({ isStretch }) => (isStretch ? "24.95em" : "15.7em")};
    height: 13em;
    text-align: center;
    @media screen and (max-width: 50em) {
        width: 100%;
        height: auto;
    }
`;
export const GlobalStyle = styled.p`
    font-size: 8rem;
    text-align: center;
    margin-bottom: 3rem;
    @media screen and (max-width: 50em) {
        display: none;
    }
`;
export const GlobalStyleForSmallScreen = styled.p`
    font-size: 10rem;
    font-weight: normal;
    text-align: right;
    display: none;
    @media screen and (max-width: 50em) {
        text-align: center;
        font-size: min(10vw, 5rem);
        display: block;
    }
`;
export const ParaStyle = styled.p`
    font-size: 1.5rem;
    @media screen and (max-width: 50em) {
        font-size: 1rem;
    }
`;
export const HStyle = styled.p`
    font-size: 3rem;
    @media screen and (max-width: 50em) {
        font-size: 2rem;
    }
`;

export const CodeSnippet = styled.div`
    width: max-content;
    height: max-content;
    display: flex;
    ${reverseTheme};
    > * {
        border: 0.25em solid currentColor;
        padding: 1em;
    }
`;
export const SmallerDiv = styled.div`
    padding: 0.5vw 0.5vw 0.5vw 0.5vw;
    display: flex;
    justify-content: center;
    align-items: center;
    @media (min-width: 50em) {
        flex: ${({ base }) => (base ? base : "40%")};
    }
`;
export const LargerDiv = styled.div`
    padding: 0.5vw 0.5vw 0.5vw 0.5vw;
    @media (min-width: 50em) {
        flex: ${({ base }) => (base ? base : "60%")};
    }
`;
