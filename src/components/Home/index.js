import { homeData } from "../../assets/constants";
import React from "react";
import Install from "../GettingStarted/install";
import { gettingStartedData } from "../../assets/constants/index";
import {
    SmallerDiv,
    LargerDiv,
    FlexContainer,
    Docs,
    Mail,
    Source,
} from "./HomeUI";
import { MainDiv, MainDivForHead, ImageStyle } from "./HomeUI";
import ExternalLink from "../UI/Utility/ExternalLink";
import ImagePortal from "./ImagePortal";
const HomeLinks = [
    {
        name: "Docs",
        link: "https://nest.nitk.ac.in/docs/",
        icon: <Docs />,
    },
    {
        name: "Source",
        link: "https://gitlab.com/nitk-nest/nest",
        icon: <Source />,
    },
    {
        name: "Mailing List",
        link: "https://groups.google.com/g/nest-users",
        icon: <Mail />,
    },
];

const Home = () => {
    const { installationData } = gettingStartedData;
    const isPhone = window.matchMedia("(max-width: 50em)").matches;
    return (
        <>
            <MainDivForHead>
                <ImageStyle src={homeData.logo} />
                <LargerDiv>
                    <FlexContainer isColumn>
                        <ImageStyle isStretch src={homeData.main} />
                        <FlexContainer>
                            {HomeLinks.map(({ name, link, icon }) => (
                                <ExternalLink key={name} link={link}>
                                    {icon}
                                    {name}
                                </ExternalLink>
                            ))}
                        </FlexContainer>
                    </FlexContainer>
                </LargerDiv>
            </MainDivForHead>
            <MainDiv>
                <LargerDiv base="70%">
                    <Install installationData={installationData} />
                </LargerDiv>
                <SmallerDiv base="30%">
                    {!isPhone && <ImagePortal src={homeData.archImage} />}
                    {isPhone && (
                        <ExternalLink link={homeData.archImage}>
                            <ImageStyle src={homeData.archImage} />
                        </ExternalLink>
                    )}
                </SmallerDiv>
            </MainDiv>
        </>
    );
};
export default Home;
