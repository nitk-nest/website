import React, { useState } from "react";
import { createPortal } from "react-dom";
import styled, { css } from "styled-components";
import { reverseTheme } from "../../assets/theme";
import colors from "../../assets/colors";
const Overlay = styled.div`
    position: fixed;
    top: 0;
    bottom: 0;
    left: 0;
    right: 0;
    background: ${colors.overlayBackground};
    z-index: 1000;
`;

const Modal = styled.div`
    position: fixed;
    top: 0;
    bottom: 0;
    left: 25%;
    right: 25%;
    z-index: 1001;
    background: ${colors.modalBackground};
`;

const Image = styled.img`
    cursor: pointer;
    width: 31em;
    height: 28em;
    ${({ full }) =>
        full &&
        css`
            cursor: unset;
            width: 100%;
            height: auto;
            max-height: 90vh;
            margin: 0 10em;
        `}
`;
const FlexContainer = styled.div`
    display: flex;
    justify-content: center;
    align-items: center;
    background: none;
    flex-direction: column;
    gap: 1em;
`;
const StyledExternalLink = styled.a`
    padding: 1em;
    text-decoration: none;
    ${reverseTheme};
`;

const ImagePortal = ({ src }) => {
    const [isOpen, setIsOpen] = useState(false);
    const Portal = createPortal(
        <>
            <Overlay onClick={() => setIsOpen(false)} />
            <Modal>
                <FlexContainer>
                    <Image full src={src} />
                    <StyledExternalLink
                        href={src}
                        rel="noreferrer noopener"
                        target="_blank"
                    >
                        View in new tab
                    </StyledExternalLink>
                </FlexContainer>
            </Modal>
        </>,
        document.getElementById("portal")
    );
    if (isOpen) return Portal;
    return <Image src={src} onClick={() => setIsOpen(true)} />;
};

export default ImagePortal;
