import React from "react";
import styled, { keyframes } from "styled-components";
import { loaderThemeColor } from "../../../assets/theme";
const spin = keyframes`
    from {
        transform: rotate(0deg);
    }

    to {
        transform: rotate(360deg);
    }`;

const Loader = styled.div`
    border: 1em solid currentColor;
    border-top: 1em solid ${loaderThemeColor};
    border-radius: 50%;
    width: 6em;
    height: 6em;
    animation: ${spin} 2s linear infinite;
`;
const Loading = () => {
    return (
        <>
            <h2>Loading</h2>
            <Loader />
        </>
    );
};

export default Loading;
