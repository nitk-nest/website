import React, { useState } from "react";
import { HashRouter, Route, Switch } from "react-router-dom";
import { ThemeProvider } from "styled-components";

import Home from "./Home";
import Contributors from "./Contributors";
import GettingStarted from "./GettingStarted";
import Events from "./Events";
import Announcements from "./Announcements";
import Header from "./UI/Header";
import Footer from "./UI/Footer";
import MainContainer from "./UI/Utility/MainContainer";
import ScrollToTop from "./Utility/ScrollToTop";
import { GlobalStyles } from "../assets/theme";

const getTheme = () => {
    const curVal = localStorage.getItem("theme");
    if (!curVal) {
        if (window.matchMedia("(prefers-color-scheme: dark)").matches) {
            localStorage.setItem("theme", "dark");
            window.location.reload();
            return "dark";
        }
        localStorage.setItem("theme", "light");
        window.location.reload();
        return "light";
    }
    // return curVal;
    return "light";
};

const App = () => {
    const [theme, setTheme] = useState(getTheme());
    const [active, setActive] = useState("/");
    const updateTheme = () => {
        const val = theme === "dark" ? "light" : "dark";
        localStorage.setItem("theme", val);
        setTheme(val);
    };
    return (
        <ThemeProvider theme={{ mode: theme }}>
            <HashRouter>
                <GlobalStyles />
                <ScrollToTop activator={(val) => setActive(val)} />
                <Header
                    themeHandler={() => updateTheme()}
                    theme={theme}
                    active={active}
                />
                <MainContainer>
                    <Switch>
                        <Route
                            path="/contributors"
                            exact
                            component={Contributors}
                        />
                        <Route
                            path="/gettingStarted"
                            exact
                            component={GettingStarted}
                        />
                        <Route path="/events" exact component={Events} />
                        <Route
                            path="/announcements"
                            exact
                            component={Announcements}
                        />
                        <Route path="/" component={Home} />
                    </Switch>
                </MainContainer>
                <Footer />
            </HashRouter>
        </ThemeProvider>
    );
};
export default App;
