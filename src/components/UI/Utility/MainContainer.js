import styled from "styled-components";

const MainContainer = styled.div`
    min-height: 90vh;
`;

export default MainContainer;
