import React from "react";
import styled from "styled-components";

const UnStyledLink = styled.a`
    text-decoration: ${({ isStyled }) => (isStyled ? "underline" : "none")};
`;

const ExternalLink = ({ link, children, isStyled }) => {
    return (
        <UnStyledLink
            href={link}
            rel="noopener noreferrer"
            target="_blank"
            isStyled={isStyled}
        >
            {children}
        </UnStyledLink>
    );
};
export default ExternalLink;
