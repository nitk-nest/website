import React from "react";
import { FlexContainer, FooterHeading } from "./UI";
import ExternalLink from "../Utility/ExternalLink";
const PeopleList = [
    { name: "Shanthanu S Rai", link: "https://github.com/shanthanu9" },
    { name: "Narayan G", link: "https://github.com/gnarayang" },
    { name: "Dhanasekhar M", link: "https://github.com/DhanaSekharM" },
    { name: "Leslie Monis" },
    {
        name: "Mohit P Tahiliani",
        link: "http://cse.nitk.ac.in/faculty/mohit-p-tahiliani",
    },
];
const People = () => {
    return (
        <FlexContainer isColumn>
            <FooterHeading>People</FooterHeading>
            {PeopleList.map(({ name, link }) => (
                <ExternalLink key={name} link={link}>
                    {name}
                </ExternalLink>
            ))}
        </FlexContainer>
    );
};
export default People;
