import React from "react";
import { FlexContainer, Linkedin, H4, LinkedInContainer } from "./UI";
import ExternalLink from "../Utility/ExternalLink";
// import { FooterContainer } from "./UI";
// import ContactUs from "./ContactUs";
// import People from "./People";
// import Resources from "./Resources";
import Copyright from "./Copyright";
const LinkedIn = () => (
    <LinkedInContainer>
        <ExternalLink link="https://www.linkedin.com/company/nitk-nest">
            <Linkedin />
            <H4>NeST</H4>
        </ExternalLink>
    </LinkedInContainer>
);
const Footer = () => {
    return (
        <FlexContainer noBottom>
            {/* <FooterContainer>
                <ContactUs />
                <People />
                <Resources />
            </FooterContainer> */}
            <Copyright />
            <LinkedIn />
        </FlexContainer>
    );
};
export default Footer;
