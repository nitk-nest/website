import React from "react";
import { H4 } from "./UI";
const Copyright = () => {
    return (
        <H4>Copyright © 2019-22 Network Stack Tester. All Rights Reserved.</H4>
    );
};
export default Copyright;
