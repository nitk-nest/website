import styled from "styled-components";
import { Linkedin as LinkedInIcon } from "@styled-icons/simple-icons";

import { headerFooterTheme as footerTheme } from "../../../assets/theme";

export const LinkedInContainer = styled.div`
    display: flex;
    align-items: flex-end;
    justify-content: flex-end;
    margin: 0 10%;
    width: min-content;
    height: 100%;
    > * {
        display: flex;
        align-items: center;
    }
`;

export const Linkedin = styled(LinkedInIcon)`
    max-width: 3em;
    padding: 0 0.5em;
    height: 3em;
`;

export const H4 = styled.h4`
    width: 100%;
    text-align: center;
    font-weight: 400;
    margin: 0;
    @media (max-width: 50em) {
        width: 90%;
        margin-inline: 5%;
    }
`;

export const FooterHeading = styled.h2`
    text-align: center;
    width: 100%;
    ${footerTheme};
`;

export const SecondLabel = styled.label`
    margin-left: 1em;
    ${footerTheme};
`;
export const MailToSection = styled.form`
    display: flex;
    flex-direction: column;
    justify-content: center;
    align-items: flex-start;
    ${footerTheme};
    label {
        padding-right: 1em;
        ${footerTheme};
    }
    input {
        padding: 0.5em;
        margin: 1em 0;
        ${footerTheme};
        border: none;
        border-bottom: 1px solid currentColor;
    }
    textarea {
        align-self: stretch;
        border: none;
        border-bottom: 1px solid currentColor;
        ${footerTheme};
    }
    a {
        text-decoration: none;
        padding: 0.5em 1em;
        ${footerTheme};
        margin: 1em 0;
        border: 1px solid currentColor;
        box-shadow: 0 0 5px 1px currentColor;
    }
`;

export const FlexContainer = styled.div`
    display: flex;
    justify-content: center;
    margin-bottom: ${({ noBottom }) => (noBottom ? "0" : "2em")};
    align-items: center;
    flex-direction: ${({ isColumn }) => (isColumn ? "column" : "row")};
    ${footerTheme};
    padding: 1em 0;
    margin-top: 2em;
    border-top: 0.2em solid currentColor;
    gap: 1em;
    > * {
        flex: 1;
        ${footerTheme};
    }
    @media (max-width: 50em) {
        flex-direction: column;
    }
`;

export const FooterContainer = styled(FlexContainer)`
    > * {
        min-height: 5vh;
        margin-inline: 5em;
        width: 100%;
        ${footerTheme};
        @media (max-width: 50em) {
            margin-inline: 2em;
            width: 80%;
        }
    }
    width: 100%;
    ${footerTheme};
`;
