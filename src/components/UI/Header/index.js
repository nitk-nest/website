import React, { useState } from "react";
import { headerData as header } from "../../../assets/constants";

import {
    MenuLink,
    MenuItem,
    Navbar,
    Menu,
    Hamburger,
    StyledSun as Sun,
    StyledMoon as Moon,
} from "./UI";
import ExternalLink from "../Utility/ExternalLink";

let MenuLinks = header.links;

const setActive = (activeIndex) => {
    MenuLinks = MenuLinks.map((item) => {
        item["active"] = false;
        return item;
    });
    MenuLinks[activeIndex].active = true;
};

const Header = ({ themeHandler, theme, active }) => {
    const [isOpen, setIsOpen] = useState(false);
    let activeIndex = MenuLinks.findIndex(({ link }) => link === active);
    const notFound = -1;
    const base = 0;
    if (activeIndex === notFound) activeIndex = base;
    setActive(activeIndex);
    return (
        <Navbar>
            <Hamburger onClick={() => setIsOpen(!isOpen)}>
                <span />
                <span />
                <span />
            </Hamburger>
            <Menu isOpen={isOpen}>
                <MenuItem isIcon key="theme">
                    {theme === "dark" ? (
                        <Sun onClick={themeHandler} />
                    ) : (
                        <Moon onClick={themeHandler} />
                    )}
                </MenuItem>
                {MenuLinks.map(({ name, link, isExternal, active }) => {
                    if (!isExternal)
                        return (
                            <MenuItem key={name} active={active}>
                                <MenuLink to={link}>{name}</MenuLink>
                            </MenuItem>
                        );
                    return (
                        <MenuItem key={name}>
                            <ExternalLink link={link}>{name}</ExternalLink>
                        </MenuItem>
                    );
                })}
            </Menu>
        </Navbar>
    );
};

export default Header;
