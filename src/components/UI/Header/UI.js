import { Link } from "react-router-dom";
import styled, { css } from "styled-components";
import { Sun } from "@styled-icons/boxicons-regular/Sun";
import { Moon } from "@styled-icons/boxicons-regular/Moon";
import theme from "styled-theming";
import colors from "../../../assets/colors";
import { headerFooterTheme as headerTheme } from "../../../assets/theme";

const { headerHoverDark, headerHoverLight } = colors;

const hoverTheme = theme("mode", {
    light: css`
        color: ${headerHoverLight};
    `,
    dark: css`
        color: ${headerHoverDark};
    `,
});

export const StyledSun = styled(Sun)`
    width: 2em;
    padding-inline: 1em;
    ${headerTheme};
    > * {
        ${headerTheme};
        &:hover {
            ${hoverTheme};
        }
    }
`;
export const StyledMoon = styled(Moon)`
    padding-inline: 1em;
    width: 2em;
    ${headerTheme};
    > * {
        ${headerTheme};
    }
    &:hover > * {
        ${hoverTheme};
    }
`;
export const MenuItem = styled.div`
    display: ${({ isIcon }) => (isIcon ? "none" : "flex")};
    align-items: center;
    flex-basis: ${({ isIcon }) => (isIcon ? null : "100%")};
    flex-grow: 1;
    justify-content: center;
    font-size: 1rem;
    min-height: 5vh;
    > * {
        ${({ active }) => (!active ? headerTheme : null)};
        display: flex;
        justify-content: center;
        align-items: center;
        text-decoration: none;
        width: 100%;
        height: 100%;
    }
    ${({ active }) => (!active ? headerTheme : null)};
    border: null;
    border-bottom: ${({ active }) =>
        active ? "2px solid currentColor" : null};
    @media (max-width: 50em) {
        padding: 1rem;
        width: 100vw;
    }
    cursor: pointer;
    :hover {
        ${hoverTheme};
        > * {
            ${hoverTheme};
        }
    }
`;

export const MenuLink = styled(Link)`
    text-align: center;
`;

export const Navbar = styled.div`
    padding-inline: 2em;
    @media (max-width: 50em) {
        padding-inline: 0;
    }
    margin: 0;
    z-index: 1000;
    flex-wrap: wrap;
    ${headerTheme};
    top: 0;
`;

export const Menu = styled.div`
    display: flex;
    justify-content: center;
    align-items: center;
    ${headerTheme};
    width: 100%;
    position: relative;
    > * {
        height: 3.5rem;
    }
    @media (max-width: 50em) {
        overflow: hidden;
        flex-direction: column;
        transition: max-height 0.3s ease-in;
        width: 100vw;
        max-height: ${({ isOpen }) => (isOpen ? "40em" : "0")};
    }
`;

export const Hamburger = styled.div`
    display: none;
    padding: 1rem;
    float: right;
    cursor: pointer;
    ${headerTheme};
    span {
        ${headerTheme};
        background: currentColor;
        height: 0.01em;
        width: 1.5em;
        margin-bottom: 0.25em;
        border-radius: 0.25em;
    }
    @media (max-width: 50em) {
        display: flex;
        flex-direction: column;
    }
`;
